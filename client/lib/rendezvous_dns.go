package snowflake_client

import (
	"encoding/hex"
	"fmt"
	"net"

	"github.com/refraction-networking/conjure/pkg/registrars/dns-registrar/requester"
	"github.com/refraction-networking/conjure/pkg/registrars/dns-registrar/tworeqresp"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/common/messages"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/common/nat"
)

// makeEncPollReq returns an encoded client poll request containing a given
// offer.
func makeEncPollReqPub(offer string) []byte {
	encPollReq, err := (&messages.ClientPollRequest{
		Offer: offer,
		NAT:   nat.NATUnknown,
	}).EncodeClientPollRequest()
	if err != nil {
		panic(err)
	}
	return encPollReq
}

// makeEncPollResp returns an encoded client poll response with given answer and
// error strings.
func makeEncPollRespPub(answer, errorStr string) []byte {
	encPollResp, err := (&messages.ClientPollResponse{
		Answer: answer,
		Error:  errorStr,
	}).EncodePollResponse()
	if err != nil {
		panic(err)
	}
	return encPollResp
}

var FakeEncPollReq = makeEncPollReqPub(`{"type":"offer","sdp":"test"}`)
var FakeEncPollResp = makeEncPollRespPub(
	`{"answer": "{\"type\":\"answer\",\"sdp\":\"fake\"}" }`,
	"",
)

const key = "0b63baad7f2f4bb5b547c53adc0fbb179852910607935e6f4b5639fd989b1156"
const bufSize = 8192

type dnsRendezvous struct {
	requester *tworeqresp.Requester
}

func newDNSRendezvous(remoteAddr string, baseDomain string) (*dnsRendezvous, error) {
	addr, err := net.ResolveUDPAddr("udp", remoteAddr)
	if err != nil {
		return nil, err
	}

	pubKey, err := hex.DecodeString(key)
	if err != nil {
		return nil, err
	}

	req, err := requester.NewRequester(&requester.Config{
		TransportMethod: requester.UDP,
		Target:          addr.String(),
		BaseDomain:      baseDomain,
		Pubkey:          pubKey,
	})
	if err != nil {
		return nil, err
	}

	tworeq, err := tworeqresp.NewRequester(req, 80)
	if err != nil {
		return nil, err
	}

	return &dnsRendezvous{
		requester: tworeq,
	}, nil
}

func (r *dnsRendezvous) Exchange(encPollReq []byte) ([]byte, error) {
	resp, err := r.requester.RequestAndRecv(encPollReq)
	if err != nil {
		return nil, fmt.Errorf("error during dns request: %v", err)
	}

	return resp, nil
}
