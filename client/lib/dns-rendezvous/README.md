# EECS 598-9 final project: DNS rendezvous for Snowflake

This is a proof of concept for DNS rendezvous method in Snowflake. The code is modified from [Conjure DNS registration](https://github.com/refraction-networking/conjure/tree/master/pkg/registrars/dns-registrar) and [DNSTT](https://www.bamsoftware.com/software/dnstt/). The biggest change from Conjure registration is that it supports arbitrary large request messages to support the larger Snowflake rendezvous message by using a lightweight fragmentation mechanism.

## Running the proof of concept

Server:

```
go run examples/server/main.go
```

Client:

```
go run examples/client/main.go
```

To view how the DNS packets are encoded, use the following command to run a network capture:

```
tcpdump -p udp -i lo -w capture.pcap
```

And after running the server and client, you will be able to open the pcap file and see the packets using [wireshark](https://www.wireshark.org/).
