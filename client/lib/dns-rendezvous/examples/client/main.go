package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"net"

	"github.com/pion/dtls/v2/examples/util"
	sf "gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/client/lib"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/client/lib/dns-rendezvous/requester"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/v2/client/lib/dns-rendezvous/tworeqresp"
)

const key = "0b63baad7f2f4bb5b547c53adc0fbb179852910607935e6f4b5639fd989b1156"
const bufSize = 8192

func main() {
	var remoteAddr = flag.String("saddr", "127.0.0.1:6666", "remote address")
	var baseDomain = flag.String("domain", "test.xyz", "base domain to use")
	flag.Parse()

	addr, err := net.ResolveUDPAddr("udp", *remoteAddr)
	util.Check(err)

	pubKey, err := hex.DecodeString(key)
	util.Check(err)

	req, err := requester.NewRequester(&requester.Config{
		TransportMethod: requester.UDP,
		Target:          addr.String(),
		BaseDomain:      *baseDomain,
		Pubkey:          pubKey,
	})

	util.Check(err)

	tworeq, err := tworeqresp.NewRequester(req, 80)
	util.Check(err)

	resp, err := tworeq.RequestAndRecv(sf.FakeEncPollReq)
	util.Check(err)

	fmt.Printf("Got message: %s\n", string(resp))

}
